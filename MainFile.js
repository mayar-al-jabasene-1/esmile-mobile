import React from "react";
import { Provider, useSelector } from "react-redux"; // Import the Provider component
import { StatusBar } from "expo-status-bar";
import { StyleSheet, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import SplashScreen from "./screens/SplashScreen";
import { Fragment, useEffect, useState } from "react";
import Login from "./screens/Login";
import Register from "./screens/Register";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { LogBox } from "react-native";
import DrawerNavigation from "./navigation/DrawerNavigation";
import Store from "./store"; // Import your Redux store
LogBox.ignoreLogs([
  "Non-serializable values were found in the navigation state",
]);

const MainFile = () => {
  const [isSplashReady, setIsSplashReady] = useState(false);
  const [userLog, setuserLog] = useState(null);
  const Stack = createNativeStackNavigator();

  let { authdata } = useSelector((state) => state.auth);

  useEffect(() => {
    // Simulating some asynchronous tasks
    const splashTimer = setTimeout(() => {
      setIsSplashReady(true);
    }, 5000);

    // Cleanup the timer if the component unmounts
    return () => clearTimeout(splashTimer);
  }, []);

  return (
    <Provider store={Store}>
      {/* Wrap your entire App component with the Provider */}
      <Fragment>
        {!isSplashReady ? (
          <View>
            <SplashScreen />
          </View>
        ) : authdata ? (
          <NavigationContainer>
            <DrawerNavigation />
          </NavigationContainer>
        ) : (
          <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
              <Stack.Screen
                name="Login"
                component={Login}
                initialParams={{ userLog, setuserLog }}
              />
              <Stack.Screen
                name="Register"
                component={Register}
                initialParams={{ userLog, setuserLog }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        )}
        <StatusBar style="auto" />
      </Fragment>
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default MainFile;
