import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";
import { MaterialIcons } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { globalstyle } from "../styles/GlobalStyle";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";

const Header = () => {
  let { authdata, loading } = useSelector((state) => state.auth);
  const navigation = useNavigation();
  return (
    <View
      style={style.header}
      className="flex-row justify-between items-center px-4"
    >
      <View>
        <MaterialIcons
          name="menu-open"
          size={24}
          color="#333"
          onPress={() => navigation.openDrawer()}
        />
      </View>
      <View style={style.test}>
        <Ionicons name="notifications" size={24} color="#333" />
        <View
          style={{ backgroundColor: globalstyle.Maincolor.color }}
          className="overflow-hidden rounded-full h-10 w-10 item-center  border border-neutral-400"
        >
          {authdata && authdata[0].user_picture ? (
            <Image
              source={{
                uri: `http://192.168.43.4:8000/storage/${authdata[0].user_picture}`,
              }}
              className="rounded-2xl h-12 w-10 "
            />
          ) : authdata && authdata[0].gender === "male" ? (
            <Image
              source={require("../assets/images/profile-image-man.png")}
              className="rounded-2xl h-12 w-10 "
            />
          ) : (
            <Image
              source={require("../assets/images/profile-image-woman.png")}
              className="rounded-2xl h-12 w-10 "
            />
          )}
        </View>
      </View>
    </View>
  );
};

let style = StyleSheet.create({
  header: {
    backgroundColor: "white",
    paddingTop: 10,
    height: 80,
    alignItems: "center",
    justifyContent: "space-between",
  },
  test: {
    flexDirection: "row",
    justifyContent: "space-around",
    gap: 15,
    alignItems: "center",
  },
});

export default Header;
