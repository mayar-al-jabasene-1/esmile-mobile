import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  Dimensions,
  Image,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";
import { globalstyle } from "../styles/GlobalStyle";
import { AntDesign } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

var { width, height } = Dimensions.get("window");
const Clinic = ({ title, data, search }) => {
  let navigation = useNavigation();
  let [show, setShow] = useState(10);
  let newdata = data && data.slice(0, show);
  let showmore = () => setShow((show += 10));
  return (
    <View className={`${search ? "" : "mt-2"} px-4`}>
      {title && (
        <Text
          className="mb-4 font-medium text-2xl "
          style={globalstyle.Maincolor}
        >
          {title && title}
        </Text>
      )}

      <SafeAreaView style={{ paddingBottom: 50, marginTop: 10 }}>
        <View>
          {data &&
            newdata.map((e, index) => {
              return <Item item={e} key={index} navigation={navigation} />;
            })}
        </View>
        {show < data.length && (
          <View style={{ alignItems: "center", paddingBottom: 30 }}>
            <TouchableOpacity
              onPress={showmore}
              style={{
                borderColor: globalstyle.Maincolor.color,
                borderWidth: 1,
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 5,
                width: "100%",
              }}
            >
              <Text
                style={{
                  color: globalstyle.Maincolor.color,
                  textAlign: "center",
                }}
              >
                Show More
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaView>
    </View>
  );
};

let Item = ({ title, navigation, item: data }) => {
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate("Profile", { id: data.id, data })}
    >
      <View
        className="bg-white  p-2 border border-gray-200  flex-row gap-2 mb-4 item-center justify-center"
        style={{ borderRadius: 30, alignItems: "center" }}
      >
        <View
          className="overflow-hidden  rounded-full h-35 w-35 item-center  border border-neutral-400"
          style={{
            backgroundColor: globalstyle.Maincolor.color,
            paddingTop: 2,
            flexDirection: "column",
            justifyContent: "flex-end",
            alignItems: "flex-end",
          }}
        >
          <Image
            source={{
              uri: `http://192.168.43.4:8000/storage/${data.doctor_picture}`,
            }}
            className="rounded-3xl mb-1"
            style={{
              width: width * 0.21,
              height: height * 0.096,
            }}
          />
        </View>
        <View className="title">
          <View className="sub-title flex-row justify-between  item-center ">
            <Text
              className="text font-semibold"
              style={{ fontSize: 21, color: globalstyle.Maincolor.color }}
            >
              {data && data.first_name + " " + data.last_name}
            </Text>
          </View>
          <Text className="mt-3">{data && data.competence_type}</Text>
          <View className="p-2">
            <View className="star flex-row justify-start item-center gap-1">
              <Text className="text-sm" style={{ fontSize: 11, color: "gray" }}>
                {data && data.years_of_experience}+years of experience
              </Text>
              <AntDesign name="star" size={15} color="yellow" />
              <AntDesign name="star" size={15} color="yellow" />
              <AntDesign name="star" size={15} color="yellow" />
              <AntDesign name="star" size={15} color="yellow" />
              <AntDesign name="star" size={15} color="yellow" />
            </View>
          </View>
          <View className="details">
            <View className="flex-row mb-1 mt-1">
              <Entypo name="location-pin" size={15} color="gray" />
              <Text style={{ color: "gray" }}>
                {data && data.location} , {data && data.location_details}
              </Text>
            </View>
          </View>
          <View className="details">
            <View className="flex-row justify-center item-center gap-1 mb-1 ">
              <AntDesign name="calendar" size={15} color="gray" />
              <Text style={{ color: "gray", marginRight: 5 }}>
                {" "}
                {data && data.start_day} _ {data && data.end_day}
              </Text>
              <AntDesign name="clockcircleo" size={15} color="gray" />
              <Text style={{ color: "gray", fontSize: 12 }}> 10Am , 3Pm</Text>
            </View>
            <TouchableOpacity
              style={{
                backgroundColor: globalstyle.Maincolor.color,
                paddingVertical: 5,
                paddingHorizontal: 40,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: "lightgray",
                alignSelf: "flex-start",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ color: "white", fontSize: 12 }}>open</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Clinic;
