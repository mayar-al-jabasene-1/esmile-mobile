import React, { useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontAwesome5 } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { globalstyle } from "../styles/GlobalStyle";
import { FontAwesome } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import { logoutfn } from "../store/authSlice";

const CustomDrawerContent = () => {
  let [login, setlogin] = useState(true);
  const navigation = useNavigation();

  const handleGoToProfile = () => {
    navigation.navigate("Myscreen"); // Navigate to the Myscreen component
  };

  let { authdata } = useSelector((state) => state.auth);
  let dispatch = useDispatch();

  const RenderImage = () => {
    if (authdata && authdata[0].user_picture) {
      // If authdata.user_picture is truthy, display the user's image
      return (
        <Image
          source={{ uri: authdata[0].user_picture }} // Use the user's picture from authdata
          style={{
            borderRadius: 10, // Add any styles you need
            width: 100,
            height: 100,
          }}
        />
      );
    } else {
      // If authdata.user_picture is falsy or missing, display a default image based on gender
      const gender = authdata[0].gender;
      if (gender === "male") {
        return (
          <Image
            source={require("../assets/images/profile-image-man.png")}
            style={{
              borderRadius: 10, // Add any styles you need
              width: 100,
              height: 100,
            }}
          />
        );
      } else if (gender === "female") {
        return (
          <Image
            source={require("../assets/images/profile-image-woman.png")}
            style={{
              borderRadius: 10, // Add any styles you need
              width: 100,
              height: 100,
            }}
          />
        );
      } else {
        // If gender is neither "male" nor "female", you can display a default image here.
        return <Text>{authdata && authdata[0].first_name}</Text>;
      }
    }
  };

  return (
    <View
      style={{
        flexDirection: "column",
        justifyContent: "space-between",
        alignItems: "flex-start",
        height: "100%",
      }}
    >
      <View style={styles.container}>
        <TouchableOpacity onPress={handleGoToProfile}>
          <View
            style={{
              backgroundColor: "gray",
              marginLeft: 20,
              padding: 10,
              borderColor: globalstyle.Maincolor.color,
              borderWidth: 1,
              marginBottom: 10,
            }}
            className="overflow-hidden rounded-full h-20 w-20 items-center  border border-neutral-400"
          >
            <RenderImage />
          </View>
        </TouchableOpacity>
        <View className="title flex-row mt-4 mb-12  border-b border-[#DFE5E3]  px-4  items-center">
          <Text
            style={{
              fontSize: 21,
              color: "#6C6C6C",
              fontWeight: "bold",
              justifyContent: "space-between",
              width: 220,
            }}
          >
            {authdata && authdata[0].first_name + " " + authdata[0].last_name}
          </Text>
          <FontAwesome5
            name="pencil-alt"
            size={16}
            color="#6C6C6C"
            style={{
              borderLeftColor: "#DFE5E3",
              borderLeftWidth: 2,
              paddingLeft: 10,
            }}
          />
        </View>
        <View className="px-4">
          <Text
            style={{
              color: "#6C6C6C",
              fontSize: 19,
              fontWeight: "700",
            }}
          >
            Go To Chat
          </Text>
        </View>
        <View className="signout flex-row items-center gap-2 px-4 mt-2">
          <Ionicons name="exit-outline" size={20} color="#6C6C6C" />
          <TouchableOpacity
            onPress={() => dispatch(logoutfn())}
            style={{
              color: "#6C6C6C",
              fontSize: 17,
            }}
          >
            <Text>Sign Out</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate("Settings")}
        style={{
          width: "100%",
        }}
      >
        <View
          className="px-4 my-5 flex-row justify-start items-center pt-2 gap-2"
          style={{
            borderTopColor: "#707070",
            borderTopWidth: 1,
          }}
        >
          <FontAwesome name="gear" size={24} color="#707070" />
          <Text style={{ fontSize: 21, fontWeight: "bold", color: "#707070" }}>
            setting
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
  },
});

export default CustomDrawerContent;
