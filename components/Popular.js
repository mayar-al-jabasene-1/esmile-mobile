import { View, Text, Dimensions, Image, TouchableOpacity } from "react-native";
import React from "react";
import Carousel from "react-native-snap-carousel";
import { globalstyle } from "../styles/GlobalStyle";
import { AntDesign } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

var { width, height } = Dimensions.get("window");
const Popular = ({ title, data }) => {
  console.log("data==>", data);
  let navigation = useNavigation();
  return (
    <View className="mt-5 px-4">
      <Text
        className="mb-4 font-medium text-2xl "
        style={globalstyle.Maincolor}
      >
        {title}
      </Text>
      <Carousel
        data={data && data}
        firstItem={1}
        renderItem={({ item }) => (
          <Teading item={item} navigation={navigation} />
        )}
        sliderWidth={width}
        inactiveSlideOpacity={0.6}
        itemWidth={width * 0.7}
        slideStyle={{ display: "flex", alignItems: "center" }}
      />
    </View>
  );
};

let Teading = ({ navigation, item: data }) => {
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate("Profile", { id: data.id, data })}
    >
      <View className=" rounded-2xl border border-gray-300 pt-2 pr-4 pl-4 w-100 pb-2 bg-white ">
        <View className="flex-row mb-2 justify-between items-center gap-3">
          <View
            style={{ backgroundColor: globalstyle.Maincolor.color }}
            className="overflow-hidden rounded-full h-20 w-30 item-center  border border-neutral-400"
          >
            {data.doctor_picture ? (
              <Image
                source={{
                  uri: `http://192.168.43.4:8000/storage/${data.doctor_picture}`,
                }}
                onError={(error) => console.error("Image Error:", error)}
                className="rounded-3xl mb-1"
                style={{ width: width * 0.2, height: height * 0.12 }}
              />
            ) : data.gender === "male" ? (
              <Image
                source={require("../assets/images/profile-image-man.png")}
                onError={(error) => console.error("Image Error:", error)}
                className="rounded-3xl mb-1"
                style={{ width: width * 0.2, height: height * 0.12 }}
              />
            ) : (
              <Image
                source={require("../assets/images/profile-image-woman.png")}
                onError={(error) => console.error("Image Error:", error)}
                className="rounded-3xl mb-1"
                style={{ width: width * 0.2, height: height * 0.12 }}
              />
            )}
          </View>
          <View>
            <Text className="text-xl font-semibold text-gray-700">
              {data && data.first_name + " " + data.last_name}
            </Text>
            <View className="flex-row  justify-between items-center gap-5">
              <Text className="text-lg" style={globalstyle.Maincolor}>
                Orthodontics
              </Text>
              <Text className=" text-gray-700">
                4 <AntDesign name="staro" size={16} color="gray" />
              </Text>
            </View>
          </View>
        </View>
        <Text className="text-gray-700">Date And Time</Text>
        <View className="flex-row  justify-between items-center gap-1">
          <View className="flex-row  justify-between items-center gap-1">
            <MaterialIcons
              name="date-range"
              size={18}
              color="rgb(156 163 175)"
            />
            <Text className="text-gray-400">sunday_thusday</Text>
          </View>
          <View className="flex-row  justify-between items-center gap-2">
            <AntDesign name="clockcircleo" size={18} color="rgb(156 163 175)" />
            <Text className="text-gray-400">10AM , 3PM</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Popular;
