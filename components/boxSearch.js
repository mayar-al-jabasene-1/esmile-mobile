import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

const BoxSearch = ({ title }) => {
  let navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate("Search")}
      className="box-search "
      style={{
        backgroundColor: "#fff",
        borderRadius: 8,
        borderColor: "lightgray",
        borderWidth: 1,
        padding: 13,
      }}
    >
      <View className="flex-row justify-start items-center gap-2">
        <AntDesign name="search1" size={18} color="gray" />
        <Text style={{ color: "gray", fontSize: 16 }}>Search For {title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default BoxSearch;
