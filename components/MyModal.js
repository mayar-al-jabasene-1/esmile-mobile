import React, { useState } from "react";
import { View, Text, Modal, TouchableOpacity, TextInput } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import { AntDesign } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import { globalstyle } from "../styles/GlobalStyle";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { createappointment } from "../store/appointmentSlice";

const MyModal = ({ toggle, id }) => {
  const [modalVisible, setModalVisible] = useState(toggle);
  const [selectedDate, setSelectedDate] = useState("");
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [selectedTime, setSelectedTime] = useState("");
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [inputsSelectedDate, setInputsSelectedDate] = useState(false);
  const [inputsSelectedTime, setInputsSelectedTime] = useState(false);
  const [showViewOne, setshowViewOne] = useState(true);
  const [showViewTwo, setshowViewTwo] = useState(false);
  const [showViewThree, setshowViewThree] = useState(false);
  const [valuebtn, setvaluebtn] = useState("");
  const [ishavevalue, setishavevalue] = useState(false);
  const [backcash, setbackcash] = useState("white");
  const [backcredit, setbackcredit] = useState("white");

  let navigation = useNavigation();
  let dispatch = useDispatch();
  let { authdata } = useSelector((state) => state.auth);
  let { data: appo, loading } = useSelector((state) => state.appointmentslice);

  const handleConfirmAppointment = (e) => {
    console.log("selectedTime==>", selectedTime);
    console.log("valuebtn==>", valuebtn);
    console.log("id==>", id);
    console.log("authdata[0]", authdata[0].id);
    console.log("shm6h==>", `${selectedDate} ${selectedTime}`);
    let formattedDateTime = `${selectedDate} ${selectedTime}`;
    const appointmentData = {
      doctor_id: id, // You can set the appropriate ID here
      selected_time: formattedDateTime,
      note: "",
    };
    console.log("appointmentData==>", appointmentData);
    e.preventDefault();
    if (selectedDate && selectedTime) {
      dispatch(
        createappointment({
          data: appointmentData,
          id: authdata[0].id,
        })
      );
      navigation.navigate("Appointments");
    }
  };

  // console.log("selectedDate==>", selectedDate);
  // console.log("selectedTime==>", selectedTime);
  // console.log("inputsSelectedDate==>", inputsSelectedDate);
  // console.log("selectedTime==>", selectedTime);
  let handelActive = (e) => {
    if (e == "credit") {
      setbackcredit("#1EB290");
      setbackcash("white");
      setvaluebtn(e);
      console.log("one");
      setishavevalue(false);
    } else {
      setbackcash("#1EB290");
      setbackcredit("white");
      setvaluebtn(e);
      console.log("two");
      setishavevalue(false);
    }
  };

  const toggleModal = () => {
    setModalVisible(!modalVisible);
  };

  const handleDateChange = (event, date) => {
    setShowDatePicker(false);
    if (date) {
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Month is zero-based
      const day = date.getDate().toString().padStart(2, "0");
      setSelectedDate(`${year}-${month}-${day}`);
    }
  };

  const handleTimeChange = (event, time) => {
    setShowTimePicker(false);
    if (time) {
      const hours = time.getHours().toString().padStart(2, "0"); // Ensure 2-digit format
      const minutes = time.getMinutes().toString().padStart(2, "0"); // Ensure 2-digit format
      setSelectedTime(`${hours}:${minutes}`);
    }
  };
  const toggleTimePicker = () => {
    setShowTimePicker(!showTimePicker);
  };

  const handleConfirm = () => {
    // Handle the confirmation logic here
    if (selectedDate) {
      setInputsSelectedDate(false);
    } else {
      setInputsSelectedDate(true);
    }
    if (selectedTime) {
      setInputsSelectedTime(false);
    } else {
      setInputsSelectedTime(true);
    }

    if (selectedDate && selectedTime) {
      setshowViewOne(false);
      setshowViewTwo(true);
    }
  };
  const handleConfirmTwo = () => {
    if (valuebtn) {
      setshowViewTwo(false);
      setshowViewThree(true);
      setishavevalue(false);
    } else {
      setishavevalue(true);
    }
  };

  let handelcacel = () => {
    setshowViewOne(true);
    setModalVisible(!modalVisible);
    setSelectedDate("");
    setSelectedTime("");
    setshowViewTwo(false);
    setshowViewThree(false);
    setvaluebtn("");
  };
  return (
    <View style={{ flex: 1 }}>
      <TouchableOpacity onPress={toggleModal}>
        <Text style={{ textAlign: "center" }}>Book Now</Text>
      </TouchableOpacity>

      <Modal
        visible={modalVisible}
        animationType="slide"
        transparent={true}
        onRequestClose={toggleModal}
      >
        {showViewOne && (
          <View className="relative" style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <TouchableOpacity style={styles.closeIcon} onPress={toggleModal}>
                <AntDesign name="close" size={24} color="black" />
              </TouchableOpacity>
              <Text style={styles.formTitle}>Choose Your Appointment Time</Text>

              <View style={styles.inputContainer}>
                <TouchableOpacity onPress={() => setShowDatePicker(true)}>
                  <Text style={styles.input}>
                    {selectedDate || "Choose Date"}
                  </Text>
                </TouchableOpacity>
                {showDatePicker && (
                  <DateTimePicker
                    value={new Date()}
                    mode="date"
                    display="default"
                    onChange={handleDateChange}
                  />
                )}
                {inputsSelectedDate && (
                  <Text style={styles.errorText}>Please select a date</Text>
                )}
              </View>

              <View style={styles.inputContainer}>
                <TouchableOpacity onPress={toggleTimePicker}>
                  <Text style={styles.input}>
                    {selectedTime || "Choose Time"}
                  </Text>
                </TouchableOpacity>
                {showTimePicker && (
                  <DateTimePicker
                    value={new Date()}
                    mode="time"
                    display="default"
                    onChange={handleTimeChange}
                  />
                )}
                {inputsSelectedTime && (
                  <Text style={styles.errorText}>Please select a time</Text>
                )}
              </View>

              <TouchableOpacity onPress={handleConfirm}>
                <View style={styles.confirmButton}>
                  <Feather name="arrow-right" size={24} color="white" />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}

        {showViewTwo && (
          <View className="relative" style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <TouchableOpacity style={styles.closeIcon} onPress={toggleModal}>
                <AntDesign name="close" size={24} color="black" />
              </TouchableOpacity>
              <Text style={styles.formTitle}>Choose the payment method</Text>

              <View style={styles.inputContainer}>
                <TouchableOpacity onPress={() => handelActive("credit")}>
                  <Text
                    style={{
                      paddingHorizontal: 20,
                      paddingVertical: 10,
                      backgroundColor: backcredit,
                      borderColor: "gray",
                      borderWidth: 1,
                      textAlign: "center",
                      borderRadius: 5,
                      color: backcredit === "white" ? "#1EB290" : "white",
                    }}
                  >
                    Card payment
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={styles.inputContainer}>
                <TouchableOpacity onPress={() => handelActive("cash")}>
                  <Text
                    style={{
                      paddingHorizontal: 20,
                      paddingVertical: 10,
                      backgroundColor: backcash,
                      borderColor: "gray",
                      borderWidth: 1,
                      textAlign: "center",
                      borderRadius: 5,
                      color: backcash === "white" ? "#1EB290" : "white",
                    }}
                  >
                    Payment is cash
                  </Text>
                </TouchableOpacity>
              </View>
              {ishavevalue && (
                <Text style={styles.errorText}>
                  Please Chose Your Payment Method
                </Text>
              )}

              <TouchableOpacity className="flex-row  justify-between items-center">
                <Text
                  onPress={() => {
                    setshowViewOne(true);
                    setshowViewTwo(false);
                  }}
                  style={globalstyle.Maincolor}
                >
                  Back
                </Text>
                <View style={styles.confirmButton}>
                  <Feather
                    name="arrow-right"
                    size={24}
                    color="white"
                    onPress={handleConfirmTwo}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}

        {showViewThree && (
          <View className="relative" style={styles.modalContainer}>
            <View
              style={{
                backgroundColor: "white",
                padding: 40,
                height: 360,
              }}
            >
              <TouchableOpacity style={styles.closeIcon} onPress={toggleModal}>
                <AntDesign name="close" size={24} color="black" />
              </TouchableOpacity>
              <Text
                className="text-center border-b pb-2 border-[lightgray] border-w-1"
                style={styles.formTitle}
              >
                Appointment Done
              </Text>

              <Text
                style={{
                  color: "gray",
                  textAlign: "center",
                  marginBottom: 20,
                }}
              >
                Your reservation has been placed at:
              </Text>

              <View className="flex-row justify-center item-center gap-1 mb-3 bg-[#1EB290] py-3 px-4 rounded-sm">
                <AntDesign name="calendar" size={15} color="#E6E6E6" />
                <Text style={{ color: "#E6E6E6", marginRight: 5 }}>
                  {selectedDate}
                </Text>
                <AntDesign name="clockcircleo" size={15} color="#E6E6E6" />
                <Text style={{ color: "#E6E6E6" }}> {selectedTime}</Text>
              </View>

              <Text style={{ color: "gray", textAlign: "center" }}>
                {valuebtn && valuebtn} payment method
              </Text>

              <View
                style={{
                  backgroundColor: "#1EB290",
                  padding: 10,
                  borderRadius: 50,
                  alignSelf: "center",
                  marginTop: 20,
                  marginBottom: 30,
                }}
              >
                <Feather name="check" size={24} color="white" />
              </View>

              <TouchableOpacity className="flex-row  justify-between items-center mt-4 gap-2">
                <Text
                  onPress={() => handelcacel()}
                  style={{
                    backgroundColor: "#ededed",
                    padding: 7,
                    borderRadius: 6,
                    color: "#444",
                    width: 140,
                    textAlign: "center",
                  }}
                >
                  cancel the appointment
                </Text>
                <Text
                  onPress={handleConfirmAppointment}
                  style={{
                    backgroundColor: globalstyle.Maincolor.color,
                    color: "white",
                    borderRadius: 6,
                    padding: 7,
                    width: 140,
                    textAlign: "center",
                    height: 50,
                    paddingTop: 15,
                  }}
                >
                  {loading ? "loading.." : "confirm"}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </Modal>
    </View>
  );
};

export default MyModal;

const styles = {
  icone: {
    top: 2,
    right: 5,
  },
  closeIcon: {
    position: "absolute",
    top: 10,
    right: 10,
  },
  formTitle: {
    fontSize: 20,
    fontWeight: "600",
    marginBottom: 20,
    color: "#444",
  },
  inputContainer: {
    marginBottom: 20,
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: "gray",
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.7)", // Semi-transparent gray background
  },
  modalContent: {
    backgroundColor: "white",
    padding: 40,
    height: 320,
  },
  confirmButton: {
    backgroundColor: "gray",
    padding: 10,
    borderRadius: 50,
    alignSelf: "flex-end",
  },
  confirmButtonText: {
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
  },
  errorText: {
    color: "red",
    marginTop: 10,
  },
};
