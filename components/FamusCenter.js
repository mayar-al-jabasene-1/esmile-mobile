import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Button,
  SafeAreaView,
  ImageBackground,
  StyleSheet,
  Dimensions,
} from "react-native";
import React, { useState } from "react";
import { Entypo } from "@expo/vector-icons";
import { globalstyle } from "../styles/GlobalStyle";
import { useNavigation } from "@react-navigation/native";

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

const FamusCenter = ({ home, data, title }) => {
  let navigation = useNavigation();
  let [show, setShow] = useState(10);

  let newdata = data.slice(0, show);

  let handelShow = () => {
    setShow((show += 10));
  };

  return (
    <View className="mt-5 px-4">
      {title && (
        <Text
          className="mb-4 font-medium text-2xl "
          style={globalstyle.Maincolor}
        >
          {title && title}
        </Text>
      )}
      <SafeAreaView style={{ paddingBottom: home ? 0 : 50 }}>
        <View>
          {data &&
            newdata.map((e, index) => {
              return (
                <Itemcenter home={home} key={index} navigation={navigation} />
              );
            })}
        </View>
        {data && show < data.length && (
          <View style={{ alignItems: "center", paddingBottom: home ? 0 : 30 }}>
            <TouchableOpacity
              onPress={handelShow}
              style={{
                borderColor: globalstyle.Maincolor.color,
                borderWidth: 1,
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 5,
                width: "100%",
              }}
            >
              <Text
                style={{
                  color: globalstyle.Maincolor.color,
                  textAlign: "center",
                }}
              >
                Show More
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </SafeAreaView>
    </View>
  );
};

let Itemcenter = ({ home, navigation }) => {
  return (
    <TouchableOpacity onPress={() => navigation.navigate("ProfileCenter")}>
      {home ? (
        <ImageBackground
          className="rounded-2xl mb-4 border border-[lightgray] border-1"
          source={require("../assets/images/background-doctor.jpg")}
          resizeMode="stretch"
          style={{
            borderRadius: 10,
            borderColor: "lightgray",
            borderWidth: 1,
          }}
        >
          <View
            className="p-3  flex-row justify-arround gap-2 items-center"
            style={{
              borderColor: `${home ? "white" : "lightgray"}`,
            }}
          >
            <View>
              <Text
                style={{
                  color: home ? "white" : `${globalstyle.Maincolor.color}`,
                  borderColor: home ? "white" : "lightgray",
                }}
                className="text-xl font-semibold mb-1 border-b  pb-1"
              >
                Al Sham Dental Center
              </Text>
              <View className="title">
                <Text
                  className="text-lg text-white mb-2 "
                  style={{
                    color: home ? "#fdfdfd" : globalstyle.Maincolor.color,
                  }}
                >
                  All specialties
                </Text>
                <Text style={{ color: home ? "#ededee" : "#555" }}>
                  Date and time :
                </Text>
                <Text style={{ color: home ? "#cdcdcd" : "lightgray" }}>
                  Sunday _ Thursday 10Am , 3Pm
                </Text>
                <Text style={{ color: home ? "#ededee" : "#555" }}>
                  address :
                </Text>
                <View className="flex-row gap-2 item-center">
                  <Text style={{ color: home ? "#cdcdcd" : "lightgray" }}>
                    <Entypo
                      name="location-pin"
                      size={16}
                      color={home ? "#cdcdcd" : "lightgray"}
                    />{" "}
                    Syria, Damascus
                  </Text>
                  <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity
                      style={{
                        backgroundColor: "transparent",
                        paddingVertical: 5,
                        paddingHorizontal: 5,
                        borderRadius: 5,
                        borderWidth: 1,
                        borderColor: home ? "#ededee" : "lightgray",
                        alignSelf: "flex-start",
                        marginBottom: 3,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Text
                        style={{
                          color: home ? "#ededee" : "gray",
                          fontSize: 12,
                        }}
                      >
                        Information
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
            <View className="image rounded-2xl">
              <Image
                source={require("../assets/images/center-1.jpg")}
                style={{
                  width: 120,
                  height: 100,
                  alignItems: "center",
                  borderRadius: 60,
                  marginTop: 20,
                }}
              />
            </View>
          </View>
        </ImageBackground>
      ) : (
        <View
          className="p-3 rounded-2xl mb-4 border border-1 flex-row justify-arround gap-2 items-center"
          style={{
            backgroundColor: "white",
            borderColor: "lightgray",
          }}
        >
          <View>
            <Text
              style={{
                color: `${globalstyle.Maincolor.color}`,
                borderColor: "lightgray",
              }}
              className="text-xl font-semibold mb-1 border-b  pb-1"
            >
              Al Sham Dental Center
            </Text>
            <View className="title">
              <Text
                className="text-lg text-white mb-2 "
                style={{
                  color: globalstyle.Maincolor.color,
                }}
              >
                All specialties
              </Text>
              <Text style={{ color: "#555" }}>Date and time :</Text>
              <Text style={{ color: "lightgray" }}>
                Sunday _ Thursday 10Am , 3Pm
              </Text>
              <Text style={{ color: "#555" }}>address :</Text>
              <View className="flex-row gap-2 item-center">
                <Text style={{ color: "lightgray" }}>
                  <Entypo name="location-pin" size={16} color="lightgray" />{" "}
                  Syria, Damascus
                </Text>
                <View style={{ flexDirection: "row" }}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: "transparent",
                      paddingVertical: 5,
                      paddingHorizontal: 5,
                      borderRadius: 5,
                      borderWidth: 1,
                      borderColor: "lightgray",
                      alignSelf: "flex-start",
                      marginBottom: 3,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Text style={{ color: "gray", fontSize: 12 }}>
                      Information
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
          <View className="image rounded-2xl">
            <Image
              source={require("../assets/images/center-1.jpg")}
              style={{
                width: 120,
                height: 100,
                alignItems: "center",
                borderRadius: 60,
                marginTop: 20,
              }}
            />
          </View>
        </View>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  img: {},
});
export default FamusCenter;
