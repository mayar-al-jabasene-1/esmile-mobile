import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./authSlice";
import doctorslice from "./DoctorSlice";
import appointmentslice from "./appointmentSlice";

const store = configureStore({
  reducer: {
    auth: authSlice, // Assuming authSlice is your reducer
    doctorslice,
    appointmentslice,
  },
});

export default store;
