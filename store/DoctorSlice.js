import axios from "axios";

const { createSlice, createAsyncThunk } = require("@reduxjs/toolkit");

export let getAllDoctors = createAsyncThunk(
  "doctors/getAllDoctors",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      let data = await axios.get(
        "http://192.168.43.4:8000/api/doctors/getAllDoctors",
        arg,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return data.data.data.data;
    } catch (error) {
      console.log("Error Response:", error.response);
      return rejectWithValue(error.message);
    }
  }
);

export let getDoctorById = createAsyncThunk(
  "doctors/getDoctorById",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      let data = await axios.get(
        `http://192.168.43.4:8000/api/doctors/getDoctorById/${arg}`,
        arg,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return data.data.data.data;
    } catch (error) {
      console.log("Error Response:", error.response);
      return rejectWithValue(error.message);
    }
  }
);

let doctorslice = createSlice({
  name: "doctor",
  initialState: {
    data: null,
    singeldata: false,
    loading: false,
    error: false,
    message: null,
    datalog: false,
  },
  reducers: {
    logoutfn: (state, action) => {
      state.authdata = null;
    },
    datalogfn: (state, action) => {
      state.datalog = action.payload;
    },
  },
  extraReducers: {
    //getAllDoctors
    [getAllDoctors.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [getAllDoctors.fulfilled]: (state, action) => {
      state.loading = false;
      state.error = false;
      state.data = action.payload;
    },
    [getAllDoctors.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    //getDoctorById
    [getDoctorById.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [getDoctorById.fulfilled]: (state, action) => {
      state.loading = false;
      state.error = false;
      state.singeldata = action.payload;
    },
    [getDoctorById.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export default doctorslice.reducer;
