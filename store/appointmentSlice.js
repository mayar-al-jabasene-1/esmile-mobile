import axios from "axios";

const { createSlice, createAsyncThunk } = require("@reduxjs/toolkit");

export let createappointment = createAsyncThunk(
  "appointments/createappointment",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    console.log("arg==", arg);
    try {
      let res = await axios.post(
        `http://192.168.43.4:8000/api/appointments/create/${arg.id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      console.log("data from appointment", res.data);
      return res.data.data;
    } catch (error) {
      console.log("Error: b6e5", error);
      console.log("Error Response:", error.response);
      return rejectWithValue(error.message);
    }
  }
);

export let getUserAppointments = createAsyncThunk(
  "getUserAppointments/appo",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    console.log("arg==?", arg);
    try {
      let data = await axios.get(
        `http://192.168.43.4:8000/api/appointments/getUserAppointments/${arg}`,
        arg,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      console.log("data from singel user ", data.data.data.data);
      return data.data.data.data;
    } catch (error) {
      rejectWithValue(error.message);
    }
  }
);

let appointmentslice = createSlice({
  name: "appointments",
  initialState: {
    data: null,
    authdata: false,
    loading: false,
    error: false,
    message: null,
    datalog: false,
  },
  reducers: {
    logoutfn: (state, action) => {
      state.authdata = null;
    },
    datalogfn: (state, action) => {
      state.datalog = action.payload;
    },
  },
  extraReducers: {
    //createappointment
    [createappointment.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
      state.data = false;
    },
    [createappointment.fulfilled]: (state, action) => {
      state.loading = false;
      state.error = false;
      state.data = action.payload;
    },
    [createappointment.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },

    //getallappointment
    [getUserAppointments.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
      state.data = false;
    },
    [getUserAppointments.fulfilled]: (state, action) => {
      state.loading = false;
      state.data = action.payload;
    },
    [getUserAppointments.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export default appointmentslice.reducer;
export let { logoutfn } = appointmentslice.actions;
