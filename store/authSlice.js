import axios from "axios";

const { createSlice, createAsyncThunk } = require("@reduxjs/toolkit");

export let reigester = createAsyncThunk(
  "users/reigester",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      let res = await axios.post(
        "http://192.168.43.4:8000/api/users/register",
        arg,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return res.data.data["0"];
    } catch (error) {
      console.log("Error Response:", error.response);
      return rejectWithValue(error.message);
    }
  }
);

export let login = createAsyncThunk("users/login", async (arg, thunkAPI) => {
  let { rejectWithValue } = thunkAPI;
  try {
    let data = await axios.post(
      "http://192.168.43.4:8000/api/users/login",
      arg,
      {
        headers: { "Content-Type": "application/json" },
      }
    );
    return data.data.data["0"];
  } catch (error) {
    console.log("Error Response:", error.response);
    return rejectWithValue(error.message);
  }
});

export let updateuser = createAsyncThunk(
  "users/updateUser",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let data = await axios.post(
        `http://192.168.43.4:8000/api/users/updateUser/${arg.id}`,
        arg.data,
        {
          headers: { "Content-Type": "application/json" },
        }
      );
      return data.data.data;
    } catch (error) {
      console.log("Error Response:", error.response);
      return rejectWithValue(error.message);
    }
  }
);

let authslice = createSlice({
  name: "auth",
  initialState: {
    data: null,
    authdata: false,
    loading: false,
    error: false,
    message: null,
    datalog: false,
  },
  reducers: {
    logoutfn: (state, action) => {
      state.authdata = null;
    },
    datalogfn: (state, action) => {
      state.datalog = action.payload;
    },
  },
  extraReducers: {
    //reigester
    [reigester.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [reigester.fulfilled]: (state, action) => {
      state.loading = false;
      state.error = false;
      state.authdata = action.payload;
    },
    [reigester.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },

    //login
    [login.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [login.fulfilled]: (state, action) => {
      state.loading = false;
      state.error = false;
      state.authdata = action.payload;
    },
    [login.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },

    //updateuser
    [updateuser.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [updateuser.fulfilled]: (state, action) => {
      state.loading = false;
      state.error = false;
      state.authdata = action.payload;
    },
    [updateuser.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export default authslice.reducer;
export let { logoutfn } = authslice.actions;
