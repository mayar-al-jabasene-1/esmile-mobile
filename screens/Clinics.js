import { View, Text, ScrollView } from "react-native";
import React, { useState } from "react";
import Clinic from "../components/Clinic";
import BoxSearch from "../components/boxSearch";
import { useSelector } from "react-redux";

const Client = () => {
  let [data, setData] = useState([
    1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6,
  ]);

  let { data: ddd } = useSelector((state) => state.doctorslice);

  return (
    <ScrollView>
      <View>
        <View className="mt-5 px-4">
          <BoxSearch title="Clinic" />
        </View>
        <Clinic data={ddd} />
      </View>
    </ScrollView>
  );
};

export default Client;
