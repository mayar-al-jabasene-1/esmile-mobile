import { View, Text, ScrollView } from "react-native";
import React, { useEffect, useState } from "react";
import Popular from "../components/Popular";
import FamusCenter from "../components/FamusCenter";
import Clinic from "../components/Clinic";
import { useDispatch, useSelector } from "react-redux";
import { getAllDoctors } from "../store/DoctorSlice";

const Home = () => {
  let { data } = useSelector((state) => state.doctorslice);
  let [dataPopular, setDatePopular] = useState([1, 2, 3, 4, 5]);
  let [dataCenter, setDateCenter] = useState([1]);
  let [dataClinic, setDateClinic] = useState([1, 2, 3, 4, 5, 6, 7]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllDoctors());
  }, []);

  return (
    <ScrollView>
      {/*start popular clinics */}
      {data ? (
        <Popular title="Popular clinics" data={data} />
      ) : (
        <Text>loading..</Text>
      )}

      {/*start Famous centers */}
      <FamusCenter home="home" title="famous centres" data={dataCenter} />

      {/*start clinics New */}
      {data ? (
        <Clinic title="clinic join new" data={data} />
      ) : (
        <Text>loading..</Text>
      )}
    </ScrollView>
  );
};

export default Home;
