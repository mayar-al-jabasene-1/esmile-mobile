import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import React, { useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { AntDesign } from "@expo/vector-icons";
import Clinic from "../components/Clinic";
import { useSelector } from "react-redux";
const Search = () => {
  let { data: ddd } = useSelector((state) => state.doctorslice);
  let [data, setData] = useState(ddd);
  let navigation = useNavigation();
  //   let dismissfn = ()=>{
  //     Keyboard.dismiss()
  //   }

  console.log("ddd", ddd);
  let getdatafilter = (inputText) => {
    // Filter the data based on the input text in the first_name field
    let newdata = ddd.filter((item) =>
      item.first_name.toLowerCase().includes(inputText.toLowerCase())
    );
    console.log("newdata ==> ", newdata);
    setData(newdata);
    // You can update the state or perform any other actions with newdata here
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View>
        <View className="mx-4 mb-3 flex-row justify-between items-center border border-[lightgray] bg-white mt-2 rounded-full">
          <TextInput
            placeholder="Search Here Please"
            placeholderTextColor={"gray"}
            className="pb-1 pl-6 flex-1 text-base font-semibold ko  tracking-wider"
            onChangeText={(e) => getdatafilter(e)}
          />
          <TouchableOpacity
            className="rounded-full p-3 m-1 bg-neutral-500"
            onPress={() => {
              navigation.goBack();
            }}
          >
            <AntDesign name="close" size={25} color="white" />
          </TouchableOpacity>
        </View>
        <Text
          className=" mx-4 font-bold text-lg mt-3 mb-4"
          style={{ color: "#444", marginBottom: -20 }}
        >
          Results {data.length}
        </Text>
        <View className=" mt-5">
          {data.length > 0 ? (
            <Clinic data={data} search="search" />
          ) : (
            <Text className=" mx-4 font-semibol text-lg mt-3 mb-4">
              There Are No Data Here
            </Text>
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Search;
