import {
  View,
  Text,
  Image,
  ScrollView,
  Button,
  ImageBackground,
} from "react-native";
import React, { useEffect, useState } from "react";
import { AntDesign } from "@expo/vector-icons";
import { globalstyle } from "../styles/GlobalStyle";
import { Entypo } from "@expo/vector-icons";
import MapView, { Marker } from "react-native-maps";
import MyModal from "../components/MyModal";
import { useDispatch, useSelector } from "react-redux";
import { getDoctorById } from "../store/DoctorSlice";

const Profile = ({ route }) => {
  let [toggle, setToggle] = useState(false);
  const { id, data: item } = route.params; // Access the id parameter

  return (
    <ScrollView>
      <View className="mt-5 px-4" style={{ paddingBottom: 70 }}>
        {/*start prfile details */}
        <View>
          <ImageBackground
            className=" mb-4 border border-[lightgray] border-1"
            source={require("../assets/images/background-doctor.jpg")}
            resizeMode="stretch"
            style={{
              borderRadius: 10,
              borderColor: "lightgray",
              borderWidth: 1,
            }}
          >
            <View
              className="card flex-row justify-start item-center p-3"
              style={{
                padding: 10,
                borderRadius: 10,
                position: "relative",
              }}
            >
              <View
                className="title flex-column gap-3"
                style={{ alignItems: "flex-start" }}
              >
                <Text className="text-2xl font-semibold mb-1 text-white text-center  border-b border-gray-200  pb-1">
                  {item && item.first_name + " " + item.last_name}
                </Text>
                <View
                  className="flex-row justify-start  mb-1"
                  style={{ alignItems: "center" }}
                >
                  <Text
                    className="mb-1"
                    style={{ color: "#fff", marginRight: 10 }}
                  >
                    Hollywood smile
                  </Text>
                  <Text
                    className="mb-1"
                    style={{ color: "#fcfcfc", fontSize: 11 }}
                  >
                    (5+years of experience)
                  </Text>
                </View>

                <View
                  className="flex-row justify-start item-center mb-1"
                  style={{ alignItems: "center" }}
                >
                  <Text
                    className="star flex-row gap-1 "
                    style={{ marginRight: 10 }}
                  >
                    <AntDesign name="star" size={15} color="yellow" />
                    <AntDesign name="star" size={15} color="yellow" />
                    <AntDesign name="star" size={15} color="yellow" />
                    <AntDesign name="star" size={15} color="yellow" />
                    <AntDesign name="star" size={15} color="yellow" />
                  </Text>
                  <Text className="" style={{ color: "#dcdcdc" }}>
                    Visitors rating
                  </Text>
                </View>
                <View
                  className="flex-row justify-start item-center mb-1"
                  style={{ alignItems: "center" }}
                >
                  <Text className="" style={{ color: "#dcdcdc" }}>
                    <Entypo name="location-pin" size={15} color={"lightgray"} />
                  </Text>
                  <Text style={{ marginLeft: 5, color: "#dcdcdc" }}>
                    {item && item.location} , {item && item.location_details}
                  </Text>
                </View>
                <Text style={{ color: "#fcfcfc", fontSize: 13 }}>
                  Book an appointment now
                </Text>
              </View>
              <View
                className="image "
                style={{ position: "absolute", right: 0, top: 32 }}
              >
                <Image
                  source={{
                    uri: `http://192.168.43.4:8000/storage/${item.doctor_picture}`,
                  }}
                  style={{
                    width: 180,
                    height: 170,
                    alignItems: "center",
                  }}
                />
              </View>
            </View>
          </ImageBackground>
        </View>

        {/*start small card */}
        <View className="small-card flex-row justify-around mt-5" style={{}}>
          <View
            style={{
              backgroundColor: "white",
              borderRadius: 10,
              textAlign: "center",
            }}
          >
            <View
              style={{
                backgroundColor: globalstyle.Maincolor.color,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                alignItems: "center",
                textAlign: "center",
                paddingHorizontal: 10,
                padding: 15,
              }}
            >
              <AntDesign
                name="user"
                size={15}
                color="white"
                style={{ textAlign: "center" }}
              />
            </View>
            <View
              style={{
                marginTop: 5,
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 10,
                textAlign: "center",
              }}
            >
              <Text style={{ textAlign: "center", color: "#555" }}>1000+</Text>
              <Text style={{ textAlign: "center", color: "#555" }}>
                patients
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: "white",
              borderRadius: 10,
              textAlign: "center",
            }}
          >
            <View
              style={{
                backgroundColor: globalstyle.Maincolor.color,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                alignItems: "center",
                textAlign: "center",
                paddingHorizontal: 10,
                padding: 15,
              }}
            >
              <AntDesign
                name="user"
                size={15}
                color="white"
                style={{ textAlign: "center" }}
              />
            </View>
            <View
              style={{
                marginTop: 5,
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 10,
                textAlign: "center",
              }}
            >
              <Text style={{ textAlign: "center", color: "#555" }}>
                {item.years_of_experience + " Years"}
              </Text>
              <Text style={{ textAlign: "center", color: "#555" }}>
                expertise
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: "white",
              borderRadius: 10,
              textAlign: "center",
            }}
          >
            <View
              style={{
                backgroundColor: globalstyle.Maincolor.color,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                alignItems: "center",
                textAlign: "center",
                paddingHorizontal: 10,
                padding: 15,
              }}
            >
              <AntDesign
                name="user"
                size={15}
                color="white"
                style={{ textAlign: "center" }}
              />
            </View>
            <View
              style={{
                marginTop: 5,
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 10,
                textAlign: "center",
              }}
            >
              <Text style={{ textAlign: "center", color: "#555" }}>5.0</Text>
              <Text style={{ textAlign: "center", color: "#555" }}>
                Evaluation
              </Text>
            </View>
          </View>
        </View>

        {/*start About */}
        <View className="mt-5" style={{ marginTop: 50 }}>
          <Text
            className="mb-3 text-3xl"
            style={{ color: globalstyle.Maincolor.color, fontWeight: "500" }}
          >
            About
          </Text>
          <Text
            style={{
              letterSpacing: 1,
              color: "#888",
              fontSize: 15,
              fontWeight: "400",
            }}
          >
            She is a distinguished doctor who is proficient in her work. She is
            characterized by lightness, speed and mastery of work
          </Text>
        </View>
        {/*start map */}
        <View className="mt-5 map" style={{ paddingBottom: 10, marginTop: 50 }}>
          <Text
            className="mb-3 text-3xl"
            style={{ color: globalstyle.Maincolor.color, fontWeight: "500" }}
          >
            Clinic site
          </Text>
          <View style={{ height: 300 }}>
            <MapView
              style={{ flex: 1 }}
              initialRegion={{
                latitude: 48.8584,
                longitude: 2.2945,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01,
              }}
            >
              <Marker
                coordinate={{ latitude: 48.8584, longitude: 2.2945 }}
                title="Eiffel Tower"
                description="The Eiffel Tower in Paris"
              />
            </MapView>
          </View>
        </View>

        {/*start button book now */}
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Text
            style={{
              color: globalstyle.Maincolor.color,
              backgroundColor: "white",
              borderColor: globalstyle.Maincolor.color,
              borderWidth: 1,
              textAlign: "center",
              paddingHorizontal: 10,
              paddingVertical: 10,
              marginTop: 7,
              paddingLeft: 30,
              paddingRight: 30,
              borderRadius: 10,
            }}
            onPress={() => setToggle(!toggle)}
          >
            <MyModal toggle={toggle} id={item.id} />
          </Text>
        </View>
      </View>
    </ScrollView>
  );
};

export default Profile;
