import { View, Text, ScrollView } from "react-native";
import React, { useState } from "react";
import FamusCenter from "../components/FamusCenter";
import BoxSearch from "../components/boxSearch";

const Centers = () => {
  let [data, setData] = useState([
    1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6,
  ]);
  return (
    <ScrollView>
      <View className="">
        <View className="mt-5 px-4">
          <BoxSearch title="center" />
        </View>
        <FamusCenter data={data} />
      </View>
    </ScrollView>
  );
};

export default Centers;
