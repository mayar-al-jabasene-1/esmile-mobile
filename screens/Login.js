import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import React, { useState } from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { Dimensions } from "react-native";
import { ImageBackground } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { LogBox } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../store/authSlice";

var { width, height } = Dimensions.get("window");
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

LogBox.ignoreLogs([
  "Non-serializable values were found in the navigation state",
]);

const Login = ({ route }) => {
  const { userLog, setuserLog } = route.params; // Access both userLog and setuserLog from route params
  let dispatch = useDispatch();
  let [show, setShow] = useState(true);

  let navigation = useNavigation();

  let reviewschema = yup.object({
    email: yup
      .string()
      .required("Email is required")
      .email("Invalid email format  You should Add like A@A")
      .matches(/@/, "Email must contain '@'"),
    password: yup
      .string()
      .required("Password is required")
      .min(8, "Password must be at least 8 characters")
      .matches(
        /^(?=.*[!@#$%^&*()_+<>?:"{},./;'[\]\\|\\-\\=]).*$/,
        "Password must contain at least one special character"
      ),
  });
  let { authdata, loading, error } = useSelector((state) => state.auth);
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <ImageBackground
        source={require("../assets/images/background-splash.jpg")}
        resizeMode="stretch"
        style={style.img}
      >
        <View className=" flex-row justify-center items-center flex-1">
          <Formik
            initialValues={{ email: "", password: "" }}
            validationSchema={reviewschema}
            onSubmit={(values) => {
              dispatch(login(values));
            }}
          >
            {(props) => (
              <View>
                <Text
                  className="text-4xl text-center "
                  style={{ color: "lightgray", fontSize: 42, marginBottom: 60 }}
                >
                  Login
                </Text>
                <TextInput
                  style={style.input}
                  placeholder="Email"
                  onChangeText={props.handleChange("email")}
                  value={props.values.email}
                  onBlur={props.handleBlur("email")}
                  placeholderTextColor={"lightgray"}
                />

                {props.touched.email && props.errors.email && (
                  <Text style={style.error}>{props.errors.email}</Text>
                )}

                <View style={{ position: "relative" }}>
                  <TextInput
                    style={style.input}
                    placeholder="Password"
                    onChangeText={props.handleChange("password")}
                    value={props.values.password}
                    onBlur={props.handleBlur("password")}
                    placeholderTextColor={"lightgray"}
                    secureTextEntry={show}
                  />

                  <AntDesign
                    name="eye"
                    size={20}
                    color="lightgray"
                    onPress={() => setShow(!show)}
                    style={{
                      position: "absolute",
                      top: 15,
                      right: 15,
                    }}
                  />
                </View>

                {props.touched.password && props.errors.password && (
                  <Text style={style.error}>{props.errors.password}</Text>
                )}
                <View
                  className="flex-row gap-2 justify-center items-center "
                  style={{ marginBottom: 40 }}
                >
                  <Text style={{ color: "#cecece", fontSize: 13 }}>
                    You do not have an account ?
                  </Text>
                  <TouchableOpacity
                    onPress={() => navigation.navigate("Register")}
                  >
                    <Text className="text-white font-bold text-center">
                      register now
                    </Text>
                  </TouchableOpacity>
                </View>

                {loading ? (
                  <Text style={style.btn}>Loading...</Text>
                ) : (
                  <Text style={style.btn} onPress={props.handleSubmit}>
                    Login
                  </Text>
                )}

                {/* Display error message if there's an error */}
                {error && (
                  <Text className="mt-4" style={style.error}>
                    {error}
                  </Text>
                )}
              </View>
            )}
          </Formik>
        </View>
      </ImageBackground>
    </TouchableWithoutFeedback>
  );
};

let style = StyleSheet.create({
  img: {
    height: screenHeight,
    width: screenWidth,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    borderWidth: 1,
    borderColor: "#ddd",
    color: "#ddd",
    paddingVertical: 10,
    paddingLeft: 20,
    fontSize: 18,
    borderRadius: 6,
    marginBottom: 20,
    width: 350,
    borderRadius: 40,
  },
  btn: {
    color: "#fff",
    borderRadius: 40,
    borderColor: "lightgray",
    textAlign: "center",
    borderWidth: 1,
    width: 100,
    paddingHorizontal: -5,
    paddingVertical: 8,
    alignSelf: "center",
    marginBottom: 30,
  },
  error: {
    color: "#e26363",
    fontWeight: "bold",
    marginBottom: 10,
    marginTop: -6,
    textAlign: "center",
  },
});

export default Login;
