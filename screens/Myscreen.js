import {
  View,
  Text,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";
import { globalstyle } from "../styles/GlobalStyle";
import { StyleSheet } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import { Picker } from "@react-native-picker/picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { updateuser } from "../store/authSlice";

const Myscreen = () => {
  let navigation = useNavigation();
  const [isEditable, setIsEditable] = useState(false);
  let { authdata, loading } = useSelector((state) => state.auth);
  const [fname, setFname] = useState(authdata && authdata[0].first_name);
  const [lname, setLname] = useState(authdata[0].last_name);
  const [email, setEmail] = useState(authdata[0].email);
  const [location_details, setlocation_details] = useState(
    authdata[0].location_details
  ); // You may not want to initialize the password field here
  const [phone, setPhone] = useState(
    authdata[0].phone_number && authdata[0].phone_number.toString()
  );
  const [city, setCity] = useState(authdata[0].location);
  const [birthday, setBirthday] = useState(authdata[0].birthday);
  let dataupdate = {
    first_name: fname,
    last_name: lname,
    phone_number: phone,
    location_details: location_details,
    gender: authdata[0].gender && authdata[0].gender,
    location: city,
    birthday: authdata[0].birthday && authdata[0].birthday,
    _method: "PUT",
  };

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  // State to store the selected birthday
  const [selectedDate, setSelectedDate] = useState("");

  // Function to show the birthday picker
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  // Function to hide the birthday picker
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  // Function to handle the selected birthday
  const handleConfirmDate = (date) => {
    setBirthday(date.toLocaleDateString());
  };

  const handleEdit = () => {
    setIsEditable(true);
  };
  let dispatch = useDispatch();
  return (
    <ScrollView>
      <View className="mt-20">
        <View
          style={{
            backgroundColor: "gray",
            padding: 10,
            borderColor: globalstyle.Maincolor.color,
            borderWidth: 1,
            marginBottom: 10,
            alignSelf: "center",
          }}
          className="overflow-hidden rounded-full h-20 w-20 items-center  border border-neutral-400"
        >
          <Image
            source={require("../assets/images/profile-image-man.png")}
            className="rounded-2xl h-20 w-20 "
          />
        </View>

        <View
          style={{ borderBottomWidth: 5, borderBottomColor: "#DFE5E3" }}
          className="title flex-row mt-4 mb-5 justify-center px-2 pb-10 items-center"
        >
          <View className="input-box" style={{ position: "relative" }}>
            <View className="flex-row justify-center items-center gap-2">
              <TextInput
                style={{
                  borderBottomWidth: isEditable ? 1 : 0,
                  borderBottomColor: "#6C6C6C",
                  color: "#84908D",
                  fontSize: 22,
                  paddingBottom: 10,
                  fontWeight: "bold",
                }}
                placeholder="mayar"
                value={fname}
                editable={isEditable}
                onChangeText={(text) => setFname(text)}
              />

              <TextInput
                style={{
                  borderBottomWidth: isEditable ? 1 : 0,
                  borderBottomColor: "#6C6C6C",
                  color: "#84908D",
                  fontSize: 22,
                  paddingBottom: 10,
                  fontWeight: "bold",
                }}
                placeholder="al jabasene"
                value={lname}
                editable={isEditable}
                onChangeText={(text) => setLname(text)}
              />
            </View>
            {isEditable && (
              <FontAwesome5
                name="pencil-alt"
                size={16}
                color="#6C6C6C"
                style={{
                  position: "absolute",
                  top: 10,
                  left: 248,
                }}
              />
            )}
          </View>
        </View>

        <View className="profile-data px-8 mt-8">
          <View className="input-box" style={{ position: "relative" }}>
            <Text style={style.textinp}>Email</Text>
            <TextInput
              style={style.inp}
              placeholder="mayar-ja2001@hotmail.com"
              value={email}
              editable={false} // Set the editable prop based on the state
              onChangeText={(text) => setEmail(text)} // Update the state when the text changes
            />
            {isEditable && (
              <FontAwesome5
                name="pencil-alt"
                size={16}
                color="#6C6C6C"
                style={{
                  position: "absolute",
                  top: 30,
                  right: 5,
                }}
              />
            )}
          </View>

          <View className="input-box" style={{ position: "relative" }}>
            <Text style={style.textinp}>Location Details</Text>
            <TextInput
              style={style.inp}
              placeholder="Rukn al den"
              value={location_details}
              editable={isEditable}
              onChangeText={(text) => setlocation_details(text)}
            />
            {isEditable && (
              <FontAwesome5
                name="pencil-alt"
                size={16}
                color="#6C6C6C"
                style={{
                  position: "absolute",
                  top: 30,
                  right: 5,
                }}
              />
            )}
          </View>

          <View className="input-box" style={{ position: "relative" }}>
            <Text style={style.textinp}>Phone</Text>
            <TextInput
              style={style.inp}
              placeholder="012516606"
              value={phone}
              editable={isEditable} // Set the editable prop based on the state
              onChangeText={(text) => setPhone(text)} // Update the state when the text changes
            />
            {isEditable && (
              <FontAwesome5
                name="pencil-alt"
                size={16}
                color="#6C6C6C"
                style={{
                  position: "absolute",
                  top: 30,
                  right: 5,
                }}
              />
            )}
          </View>

          <View className="input-box" style={{ position: "relative" }}>
            <Text style={style.textinp}>City</Text>
            <View style={style.pickerContainer}>
              <Picker
                style={style.picker}
                selectedValue={city}
                enabled={isEditable} // Set the enabled prop based on the isEditable state
                onValueChange={(itemValue) => setCity(itemValue)}
              >
                <Picker.Item label="Select a city" value="" />
                <Picker.Item label="Damascus" value="Damascus" />
                <Picker.Item label="Alepo" value="Alepo" />
                <Picker.Item label="Sweda" value="Sweda" />
                <Picker.Item label="Homs" value="Homs" />
                <Picker.Item label="Hamah" value="Hamah" />
                <Picker.Item label="Latakia" value="Latakia" />
                <Picker.Item label="Daraa" value="Daraa" />
                {/* Add more cities as needed */}
              </Picker>
            </View>
            {isEditable && (
              <FontAwesome5
                name="pencil-alt"
                size={16}
                color="#6C6C6C"
                style={{
                  position: "absolute",
                  top: 30,
                  right: 5,
                }}
              />
            )}
          </View>

          <View className="input-box" style={{ position: "relative" }}>
            <Text style={style.textinp}>Birthday</Text>
            <TouchableOpacity
              enabled={isEditable}
              onPress={() =>
                setDatePickerVisibility(
                  isEditable ? !isDatePickerVisible : isDatePickerVisible
                )
              }
            >
              <Text
                style={{
                  borderBottomColor: "#6C6C6C",
                  borderBottomWidth: 0.9,
                  width: 290,
                  paddingBottom: 3,
                  alignSelf: "center",
                  marginBottom: 15,
                  color: "#84908D",
                  marginTop: 8,
                }}
              >
                {birthday}
              </Text>
            </TouchableOpacity>
            {isEditable && (
              <FontAwesome5
                name="pencil-alt"
                size={16}
                color="#6C6C6C"
                style={{
                  position: "absolute",
                  top: 30,
                  right: 5,
                }}
              />
            )}
          </View>

          <DateTimePickerModal
            selectedValue={birthday}
            onConfirm={(itemValue) => {
              handleConfirmDate(itemValue);
            }}
            isVisible={isDatePickerVisible}
            mode="date"
            onCancel={hideDatePicker}
          />

          {/* The rest of the input fields go here */}
        </View>
        <View className="btn">
          {isEditable ? (
            <TouchableOpacity
              onPress={() => {
                dispatch(updateuser({ data: dataupdate, id: authdata[0].id }));
                setIsEditable(!isEditable);
              }}
              style={{
                borderRadius: 5,
                borderColor: globalstyle.Maincolor.color,
                paddingHorizontal: 30,
                paddingVertical: 10,
                marginTop: 20,
                borderWidth: 1,
                color: "#1EB290",
                alignSelf: "center",
              }}
            >
              {loading ? <Text>Loading...</Text> : <Text>Save</Text>}
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={handleEdit}
              style={{
                borderRadius: 5,
                borderColor: globalstyle.Maincolor.color,
                paddingHorizontal: 30,
                paddingVertical: 10,
                marginTop: 20,
                borderWidth: 1,
                color: "#1EB290",
                alignSelf: "center",
              }}
            >
              <Text>Edit</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
      <AntDesign
        onPress={() => navigation.goBack()}
        style={{ position: "absolute", top: 30, left: 15 }}
        name="arrowleft"
        size={32}
        color="#6C6C6C"
      />
    </ScrollView>
  );
};
let style = StyleSheet.create({
  inp: {
    borderBottomColor: "#6C6C6C",
    borderBottomWidth: 0.9,
    width: 290,
    paddingBottom: 3,
    alignSelf: "center",
    marginBottom: 15,
  },
  textinp: {
    color: "#1EB290",
    fontWeight: "bold",
    fontSize: 25,
  },
  // Custom styles for the picker
  pickerContainer: {
    borderBottomColor: "#6C6C6C",
    borderBottomWidth: 0.9,
    width: 290,
    paddingBottom: 3,
    alignSelf: "center",
    marginBottom: 15,
    backgroundColor: "#f0f0f0", // Background color for the picker
    borderRadius: 5,
  },
  picker: {
    color: "#84908D", // Color of the selected value
    height: 40,
    width: 290,
  },
});
export default Myscreen;
