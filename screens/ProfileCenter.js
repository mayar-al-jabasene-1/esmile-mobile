import {
  View,
  Text,
  Image,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";
import { LinearGradient } from "expo-linear-gradient";
import { AntDesign } from "@expo/vector-icons";
import { globalstyle } from "../styles/GlobalStyle";
import { Entypo } from "@expo/vector-icons";
import MapView, { Marker } from "react-native-maps";
import Carousel from "react-native-snap-carousel";
import { Feather } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";

var { width, height } = Dimensions.get("window");

const ProfileCenter = () => {
  let navigation = useNavigation();
  // let [data, setData] = useState([1, 2, 3, 4]);
  let { data } = useSelector((state) => state.doctorslice);
  console.log("data center ==>", data);
  const carouselRef = React.useRef(null);
  const handleSnapToItem = (index) => {
    if (index === data.length - 1) {
      setTimeout(() => {
        carouselRef.current.snapToItem(0); // Snap to the first item after a delay
      }, 3000); // Delay in milliseconds before snapping to the first item
    }
  };

  return (
    <ScrollView>
      <View className="px-4">
        {/*start box image */}
        <View
          className="section-image"
          style={{ height: 300, position: "relative" }}
        >
          <View
            className="image"
            style={{
              position: "absolute",
              top: -10,
              left: "25%",
              zIndex: 2,
            }}
          >
            <Image
              source={{
                uri: `http://192.168.43.4:8000/storage/${data[0].doctor_picture}`,
              }}
              style={{
                width: 200,
                height: 300,
                alignItems: "center",
                marginTop: 20,
              }}
            />
          </View>

          <View
            className="image"
            style={{
              position: "absolute",
              top: -20,
              left: -40,
              zIndex: 1,
            }}
          >
            <Image
              source={{
                uri: `http://192.168.43.4:8000/storage/${data[0].doctor_picture}`,
              }}
              style={{
                width: 200,
                height: 300,
                alignItems: "center",
                marginTop: 20,
              }}
            />
          </View>

          <View
            className="image"
            style={{
              position: "absolute",
              top: -20,
              right: -13,
              zIndex: 1,
            }}
          >
            <Image
              source={{
                uri: `http://192.168.43.4:8000/storage/${data[0].doctor_picture}`,
              }}
              style={{
                width: 200,
                height: 300,
                alignItems: "center",
                marginTop: 20,
              }}
            />
          </View>
          {/*
              <LinearGradient
       start={{ x: 0.5, y: 0 }}
       end={{ x: 0.5, y: 1 }}
       colors={["transparent", "rgba(23,23,23,0.8)", "rgba(23,23,23,1)"]}
       style={{ width: width, height: height * 0.4 }}
       className="absolute bottom-0"
     ></LinearGradient>
     */}
        </View>

        {/*start big title */}
        <View className="mt-5">
          <Text
            style={{
              textAlign: "center",
              color: globalstyle.Maincolor.color,
              fontWeight: "700",
              marginTop: 20,
              letterSpacing: 0.3,
            }}
            className="text-4xl"
          >
            Shafa Dental Center
          </Text>
        </View>

        {/*start small card */}
        <View className="small-card flex-row justify-around mt-5" style={{}}>
          <View
            style={{
              backgroundColor: "white",
              borderRadius: 10,
              textAlign: "center",
            }}
          >
            <View
              style={{
                backgroundColor: globalstyle.Maincolor.color,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                alignItems: "center",
                textAlign: "center",
                paddingHorizontal: 10,
                padding: 15,
              }}
            >
              <AntDesign
                name="user"
                size={15}
                color="white"
                style={{ textAlign: "center" }}
              />
            </View>
            <View
              style={{
                marginTop: 5,
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 10,
                textAlign: "center",
              }}
            >
              <Text style={{ textAlign: "center", color: "#555" }}>1000+</Text>
              <Text style={{ textAlign: "center", color: "#555" }}>
                patients
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: "white",
              borderRadius: 10,
              textAlign: "center",
            }}
          >
            <View
              style={{
                backgroundColor: globalstyle.Maincolor.color,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                alignItems: "center",
                textAlign: "center",
                paddingHorizontal: 10,
                padding: 15,
              }}
            >
              <AntDesign
                name="user"
                size={15}
                color="white"
                style={{ textAlign: "center" }}
              />
            </View>
            <View
              style={{
                marginTop: 5,
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 10,
                textAlign: "center",
              }}
            >
              <Text style={{ textAlign: "center", color: "#555" }}>5 Yrs</Text>
              <Text style={{ textAlign: "center", color: "#555" }}>
                expertise
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: "white",
              borderRadius: 10,
              textAlign: "center",
            }}
          >
            <View
              style={{
                backgroundColor: globalstyle.Maincolor.color,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                alignItems: "center",
                textAlign: "center",
                paddingHorizontal: 10,
                padding: 15,
              }}
            >
              <AntDesign
                name="user"
                size={15}
                color="white"
                style={{ textAlign: "center" }}
              />
            </View>
            <View
              style={{
                marginTop: 5,
                paddingLeft: 20,
                paddingRight: 20,
                paddingBottom: 10,
                textAlign: "center",
              }}
            >
              <Text style={{ textAlign: "center", color: "#555" }}>5.0</Text>
              <Text style={{ textAlign: "center", color: "#555" }}>
                Evaluation
              </Text>
            </View>
          </View>
        </View>

        {/*start about */}

        <View className="mt-5" style={{ marginTop: 50 }}>
          <Text
            className="mb-3 text-3xl"
            style={{ color: globalstyle.Maincolor.color, fontWeight: "500" }}
          >
            About
          </Text>
          <Text
            style={{
              letterSpacing: 1,
              color: "#888",
              fontSize: 15,
              fontWeight: "400",
            }}
          >
            Our center is integrated, contains all services, and is equipped
            with the latest surgical equipment and tools that provide the best
            result that satisfies the patient Wish you all the best
          </Text>
        </View>

        {/*start carosle */}
        <View className="carosle" style={{ marginTop: 30 }}>
          <Carousel
            ref={carouselRef} // Add a ref to the carousel
            data={data}
            firstItem={1}
            renderItem={({ item }) => (
              <CardsCenter item={item} navigation={navigation} />
            )}
            sliderWidth={width}
            inactiveSlideOpacity={0.6}
            itemWidth={width * 0.5}
            slideStyle={{ display: "flex", alignItems: "center" }}
            autoplay={true} // Enable autoplay
            autoplayInterval={3000} // Set the autoplay interval (in milliseconds)
            onSnapToItem={handleSnapToItem} // Add the onSnapToItem callback
          />
        </View>

        {/*start map */}
        <View className="mt-5 map" style={{ paddingBottom: 70, marginTop: 50 }}>
          <Text
            className="mb-3 text-3xl"
            style={{ color: globalstyle.Maincolor.color, fontWeight: "500" }}
          >
            Center site
          </Text>
          <View style={{ height: 300 }}>
            <MapView
              style={{ flex: 1 }}
              initialRegion={{
                latitude: 48.8584,
                longitude: 2.2945,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01,
              }}
            >
              <Marker
                coordinate={{ latitude: 48.8584, longitude: 2.2945 }}
                title="Eiffel Tower"
                description="The Eiffel Tower in Paris"
              />
            </MapView>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

let CardsCenter = ({ navigation, item: data }) => {
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate("Profile", { id: data.id, data })}
    >
      <View
        style={{
          backgroundColor: "white",
          paddingHorizontal: 10,
          paddingVertical: 10,
          borderRadius: 10,
        }}
      >
        <View
          className="all-doctor"
          style={{
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            gap: 15,
            marginBottom: 15,
          }}
        >
          <View
            style={{
              borderColor: globalstyle.Maincolor.color,
              borderWidth: 1,
            }}
            className="overflow-hidden rounded-full h-40 w-40 item-center  border border-neutral-400"
          >
            <Image
              source={{
                uri: `http://192.168.43.4:8000/storage/${data.doctor_picture}`,
              }}
              className="rounded-3xl mb-1"
              style={{ width: width * 0.35, height: height * 0.2 }}
            />
          </View>
          <Text
            style={{
              textAlign: "center",
              color: globalstyle.Maincolor.color,
            }}
          >
            {data.first_name + " " + data.last_name}
          </Text>
          <Text
            style={{
              textAlign: "left",
              color: "#444",
              width: width * 0.3,
              letterSpacing: 1.2,
            }}
          >
            Specialization in jaw and periodontal surgery
          </Text>
        </View>
        <View>
          <Feather
            name="arrow-right"
            size={18}
            color="white"
            style={{
              backgroundColor: globalstyle.Maincolor.color,
              padding: 10,
              textAlign: "right",
              alignContent: "flex-end",
              alignSelf: "flex-end",
              borderRadius: 50,
            }}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};
export default ProfileCenter;
