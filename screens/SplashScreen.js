import {
  View,
  Text,
  Image,
  Dimensions,
  ImageBackground,
  StyleSheet,
} from "react-native";
import React from "react";

var { width, height } = Dimensions.get("window");
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

const SplashScreen = () => {
  return (
    <View>
      <ImageBackground
        source={require("../assets/images/background-splash.jpg")}
        resizeMode="stretch"
        style={styles.img}
      >
        <View className=" flex-row justify-center items-center flex-1">
          <Image
            source={require("../assets/images/logo-splash.png")}
            style={{
              marginRight: 30,
              width: width * 0.72,
              height: height * 0.22,
            }}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  img: {
    height: screenHeight,
    width: screenWidth,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 2,
    padding: 10,
  },
});
export default SplashScreen;
