import { View, Text, ScrollView, Dimensions, Image } from "react-native";
import React, { useEffect } from "react";
import { globalstyle } from "../styles/GlobalStyle";
import { AntDesign } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import { getUserAppointments } from "../store/appointmentSlice";

var { width, height } = Dimensions.get("window");

const Appointments = () => {
  let { authdata } = useSelector((state) => state.auth);
  let { error, data, loading } = useSelector((state) => state.appointmentslice);
  console.log("data==>", data);
  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserAppointments(authdata[0].id));
    console.log("123123123");
  }, []);

  return (
    <ScrollView>
      <View className="mt-5 px-4" style={{ paddingBottom: 70 }}>
        {loading ? (
          <Text>loaidng...</Text>
        ) : data && data.length > 0 ? (
          <View>
            {data &&
              data.map((e, index) => {
                const [datePart, timePart] = e.selected_time.split(" ");
                return (
                  <View
                    className="card mb-3"
                    key={index}
                    style={{
                      backgroundColor: "#fff",
                      borderColor: "lightgray",
                      borderWidth: 1,
                      borderRadius: 8,
                      padding: 15,
                    }}
                  >
                    <View className="image-title flex-row items-end gap-3 mb-4">
                      <View
                        style={{ backgroundColor: globalstyle.Maincolor.color }}
                        className="overflow-hidden rounded-full h-16 w-16 items-center  border border-neutral-400"
                      >
                        <Image
                          source={{
                            uri: `http://192.168.43.4:8000/storage/${e.doctor_picture}`,
                          }}
                          className="rounded-3xl mb-1"
                          style={{
                            width: width * 0.16,
                            height: height * 0.066,
                          }}
                        />
                      </View>
                      <View className="flex-row gap-2 items-center">
                        <Text
                          style={{
                            fontWeight: "bold",
                            fontSize: 20,
                            marginBottom: 5,
                          }}
                        >
                          {e.doctor_first_name + " " + e.doctor_last_name}
                        </Text>
                        <Text style={{ fontSize: 12, color: "#1EB290" }}>
                          Hollywood smile
                        </Text>
                      </View>
                    </View>
                    <View className="flex-row justify-center item-center gap-1 mb-4 bg-[#1EB290] py-3 px-4 rounded-md">
                      <AntDesign name="calendar" size={15} color="#E6E6E6" />
                      <Text style={{ color: "#E6E6E6", marginRight: 5 }}>
                        {datePart}
                      </Text>
                      <AntDesign
                        name="clockcircleo"
                        size={15}
                        color="#E6E6E6"
                      />
                      <Text style={{ color: "#E6E6E6" }}>{timePart}</Text>
                    </View>
                    <Text
                      style={{
                        fontSize: 17,
                        alignItems: "center",
                        color: "#1EB290",
                        alignSelf: "center",
                      }}
                    >
                      Payment is cash
                    </Text>
                  </View>
                );
              })}
          </View>
        ) : error ? (
          error.message
        ) : (
          <Text>There Are No Appointment</Text>
        )}
      </View>
    </ScrollView>
  );
};

export default Appointments;
