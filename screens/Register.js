import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Button,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import React, { useState } from "react";
import * as yup from "yup";
import { Formik } from "formik";
import { Dimensions } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { ImageBackground } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { LogBox } from "react-native";
import { RadioButton } from "react-native-paper"; // Import RadioButton from 'react-native-elements'
import { Picker } from "@react-native-picker/picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { reigester } from "../store/authSlice";
import * as ImagePicker from "expo-image-picker";
import { Permissions } from "expo";

var { width, height } = Dimensions.get("window");
const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

LogBox.ignoreLogs([
  "Non-serializable values were found in the navigation state",
]);

const Register = ({ route }) => {
  const { userLog, setuserLog } = route.params; // Access both userLog and setuserLog from route params

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  let dispatch = useDispatch();
  // State to store the selected birthday
  const [selectedDate, setSelectedDate] = useState("");

  // Function to show the birthday picker
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  // Function to hide the birthday picker
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  // Function to handle the selected birthday
  const handleConfirmDate = (date) => {
    const formattedDate = date;
    setSelectedDate(formattedDate);
  };

  let [show, setShow] = useState(true);

  let navigation = useNavigation();

  let reviewschema = yup.object({
    first_name: yup
      .string()
      .required("First Name is required")
      .min(3, "First Name must be at least 3 characters"),
    last_name: yup
      .string()
      .required("Last Name is required")
      .min(3, "Last Name must be at least 3 characters"),
    email: yup
      .string()
      .required("Email is required")
      .email("Invalid email format. Please enter a valid email.")
      .matches(/@/, "Email must contain '@'"),
    password: yup
      .string()
      .required("Password is required")
      .min(8, "Password must be at least 8 characters")
      .matches(
        /^(?=.*[!@#$%^&*()_+<>?:"{},./;'[\]\\|\\-\\=]).*$/,
        "Password must contain at least one special character"
      ),
    phone_number: yup
      .string()
      .required("Phone number is required")
      .matches(/^\d{10}$/, "Phone number must be exactly 10 digits"),
    location: yup.string().required("location is required"),
    location_details: yup.string(),
    gender: yup.string().required("Gender is required"),
    // city: yup.string().required("City is required"),
    // gender: yup.string().required("Gender is required"),
    birthday: yup.date().required("Birthday is required"),
  });
  let { authdata, loading, error } = useSelector((state) => state.auth);
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <ImageBackground
        source={require("../assets/images/background-splash.jpg")}
        resizeMode="stretch"
        style={style.img}
      >
        <View className=" flex-row justify-center items-center flex-1 ">
          <Formik
            initialValues={{
              first_name: "",
              last_name: "",
              email: "",
              password: "",
              phone_number: "",
              location: "",
              gender: "",
              birthday: selectedDate,
              location_details: "jjj", // New field
              user_picture: "",
            }}
            validationSchema={reviewschema}
            onSubmit={(values) => {
              dispatch(reigester(values));
              // setuserLog(true);
            }}
          >
            {(props) => (
              <View>
                <Text
                  className="text-4xl text-center "
                  style={{ color: "lightgray", fontSize: 42, marginBottom: 20 }}
                >
                  Register
                </Text>
                <TextInput
                  style={style.input}
                  placeholder="First Name"
                  onChangeText={props.handleChange("first_name")}
                  value={props.values.first_name}
                  onBlur={props.handleBlur("first_name")}
                  placeholderTextColor={"lightgray"}
                />
                {props.touched.first_name && props.errors.first_name && (
                  <Text style={style.error}>{props.errors.first_name}</Text>
                )}

                <TextInput
                  style={style.input}
                  placeholder="Last Name"
                  onChangeText={props.handleChange("last_name")}
                  value={props.values.last_name}
                  onBlur={props.handleBlur("last_name")}
                  placeholderTextColor={"lightgray"}
                />
                {props.touched.last_name && props.errors.last_name && (
                  <Text style={style.error}>{props.errors.last_name}</Text>
                )}

                <TextInput
                  style={style.input}
                  placeholder="Email"
                  onChangeText={props.handleChange("email")}
                  value={props.values.email}
                  onBlur={props.handleBlur("email")}
                  placeholderTextColor={"lightgray"}
                />

                {props.touched.email && props.errors.email && (
                  <Text style={style.error}>{props.errors.email}</Text>
                )}

                <View style={{ position: "relative" }}>
                  <TextInput
                    style={style.input}
                    placeholder="Password"
                    onChangeText={props.handleChange("password")}
                    value={props.values.password}
                    onBlur={props.handleBlur("password")}
                    placeholderTextColor={"lightgray"}
                    secureTextEntry={show}
                  />

                  <AntDesign
                    name="eye"
                    size={20}
                    color="lightgray"
                    onPress={() => setShow(!show)}
                    style={{
                      position: "absolute",
                      top: 15,
                      right: 15,
                    }}
                  />
                </View>

                {props.touched.password && props.errors.password && (
                  <Text style={style.error}>{props.errors.password}</Text>
                )}

                <TextInput
                  style={style.input}
                  placeholder="phone number"
                  onChangeText={props.handleChange("phone_number")}
                  value={props.values.phone_number}
                  onBlur={props.handleBlur("phone_number")}
                  placeholderTextColor={"lightgray"}
                  keyboardType="phone-pad" // Set keyboard type to phone pad
                />
                {props.touched.phone_number && props.errors.phone_number && (
                  <Text style={style.error}>{props.errors.phone_number}</Text>
                )}

                <View style={style.pickerBox}>
                  <Picker
                    style={style.picker}
                    selectedValue={props.values.location}
                    onValueChange={(itemValue) =>
                      props.setFieldValue("location", itemValue)
                    }
                  >
                    <Picker.Item label="Select a location" value="" />
                    <Picker.Item label="Damascus" value="Damascus" />
                    <Picker.Item label="Alepo" value="Alepo" />
                    <Picker.Item label="Sweda" value="Sweda" />
                    <Picker.Item label="Homs" value="Homs" />
                    <Picker.Item label="Hamah" value="Hamah" />
                    <Picker.Item label="Latakia" value="Latakia" />
                    <Picker.Item label="Daraa" value="Daraa" />
                    {/* Add more cities as needed */}
                  </Picker>
                </View>
                {props.touched.location && props.errors.location && (
                  <Text style={style.error}>{props.errors.location}</Text>
                )}

                {/* ... Your showDatePicker  inputs ... */}

                <TouchableOpacity
                  onPress={() => setDatePickerVisibility(!isDatePickerVisible)}
                >
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: "#ddd",
                      color: "#ddd",
                      paddingVertical: 15,
                      paddingLeft: 20,
                      fontSize: 18,
                      borderRadius: 6,
                      marginBottom: 20,
                      width: 350,
                      borderRadius: 40,
                    }}
                  >
                    <Text style={{ color: "lightgray" }}>
                      {selectedDate
                        ? new Date(selectedDate).toDateString()
                        : "Birthday"}
                    </Text>
                  </View>
                </TouchableOpacity>
                {props.touched.birthday && props.errors.birthday && (
                  <Text style={style.error}>{props.errors.birthday}</Text>
                )}

                <DateTimePickerModal
                  selectedValue={props.values.birthday}
                  onConfirm={(itemValue) => {
                    props.setFieldValue(
                      "birthday",
                      itemValue.toISOString().split("T")[0]
                    );
                    handleConfirmDate(itemValue.toISOString().split("T")[0]);
                  }}
                  isVisible={isDatePickerVisible}
                  mode="date"
                  onCancel={hideDatePicker}
                />
                {/* Radio Buttons */}

                <View style={{ marginBottom: 10, marginLeft: 10 }}>
                  <Text style={{ color: "white", marginBottom: 10 }}>
                    Gender:
                  </Text>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <RadioButton
                      value="male"
                      status={
                        props.values.gender === "male" ? "checked" : "unchecked"
                      }
                      onPress={() => props.setFieldValue("gender", "male")}
                    />
                    <Text style={{ color: "white", marginRight: 20 }}>
                      Male
                    </Text>

                    <RadioButton
                      value="female"
                      status={
                        props.values.gender === "female"
                          ? "checked"
                          : "unchecked"
                      }
                      onPress={() => props.setFieldValue("gender", "female")}
                    />
                    <Text style={{ color: "white" }}>Female</Text>
                  </View>
                  {/* <TouchableOpacity onPress={() => pickImage(props)}>
                    <View style={style.imagePickerButton}>
                      <Text style={style.imagePickerButtonText}>Select Image</Text>
                    </View>
                  </TouchableOpacity>
                  {props.values.user_picture && (
                    <Image
                      source={{ uri: props.values.user_picture }}
                      style={{ width: 100, height: 100 }}
                    />
                  )}*/}
                </View>

                <View
                  className="flex-row gap-2 justify-center items-center "
                  style={{ marginBottom: 40 }}
                >
                  <Text style={{ color: "#cecece", fontSize: 13 }}>
                    You have already an account ?
                  </Text>
                  <TouchableOpacity
                    onPress={() => navigation.navigate("Login")}
                  >
                    <Text className="text-white font-bold text-center">
                      Sign in
                    </Text>
                  </TouchableOpacity>
                </View>

                {loading ? (
                  <Text style={style.btn}>Loading...</Text>
                ) : (
                  <Text style={style.btn} onPress={props.handleSubmit}>
                    Register
                  </Text>
                )}

                {/* Display error message if there's an error */}
                {error && (
                  <Text className="mt-4" style={style.error}>
                    {error}
                  </Text>
                )}
              </View>
            )}
          </Formik>
        </View>
      </ImageBackground>
    </TouchableWithoutFeedback>
  );
};

let style = StyleSheet.create({
  img: {
    height: screenHeight,
    width: screenWidth,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 40,
  },
  input: {
    borderWidth: 1,
    borderColor: "#ddd",
    color: "#ddd",
    paddingVertical: 10,
    paddingLeft: 20,
    fontSize: 18,
    borderRadius: 6,
    marginBottom: 20,
    width: 350,
    borderRadius: 40,
  },
  btn: {
    color: "#fff",
    borderRadius: 40,
    borderColor: "lightgray",
    textAlign: "center",
    borderWidth: 1,
    width: 100,
    paddingHorizontal: -5,
    paddingVertical: 8,
    alignSelf: "center",
  },
  error: {
    color: "#e26363",
    fontWeight: "bold",
    marginBottom: 10,
    marginTop: -8,
    textAlign: "center",
  },
  pickerBox: {
    borderWidth: 1,
    borderColor: "#ddd",
    paddingVertical: -3,
    height: 50,
    paddingLeft: 20,
    width: 350,
    borderRadius: 40,
    marginBottom: 20,
  },
  picker: {
    color: "#ddd",
    fontSize: 10,
  },
});

export default Register;
