import { View, Text, StyleSheet } from "react-native";
import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { FontAwesome } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";

const Settings = () => {
  let navigation = useNavigation();
  return (
    <View
      style={{
        padding: 40,
        marginTop: 100,
        flexDirection: "column",
        alignItems: "flex-start",
        gap: 70,
      }}
    >
      <View style={style.title_icon} className="icon-title">
        <Ionicons name="notifications" size={19} color="#707070" />
        <Text style={style.text_tittle}>Notification tone</Text>
      </View>

      <View style={style.title_icon} className="icon-title">
        <Entypo name="language" size={19} color="#707070" />
        <Text style={style.text_tittle}>Application language</Text>
      </View>

      <View style={style.title_icon} className="icon-title">
        <Ionicons name="notifications" size={19} color="#707070" />
        <Text style={style.text_tittle}>Application evaluation</Text>
      </View>

      <View style={style.title_icon} className="icon-title">
        <FontAwesome name="star" size={19} color="#707070" />
        <Text style={style.text_tittle}>About the application</Text>
      </View>

      <View style={style.title_icon} className="icon-title">
        <Ionicons name="mail-open" size={19} color="#707070" />
        <Text style={style.text_tittle}>invite a friend</Text>
      </View>

      <View style={style.title_icon} className="icon-title">
        <AntDesign name="minuscircle" size={19} color="#707070" />
        <Text style={style.text_tittle}>Submit a complaint</Text>
      </View>

      <AntDesign
        onPress={() => navigation.goBack()}
        style={{ position: "absolute", top: -40, left: 15 }}
        name="arrowleft"
        size={32}
        color="#6C6C6C"
      />
    </View>
  );
};

let style = StyleSheet.create({
  title_icon: {
    flexDirection: "row",
    justifyContent: "flex-start",
    gap: 8,
    alignItems: "center",
  },
  text_tittle: {
    color: "#707070",
    fontSize: 24,
    fontWeight: "bold",
  },
});

export default Settings;
