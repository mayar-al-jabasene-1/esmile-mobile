import { View, Text } from "react-native";
import React from "react";
import { FontAwesome } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "../screens/Home";
import Client from "../screens/Clinics";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Centers from "../screens/Centers";
import Appointments from "../screens/Appointments";
import Header from "../components/Header";
import { globalstyle } from "../styles/GlobalStyle";
import Profile from "../screens/Profile";
import ProfileCenter from "../screens/ProfileCenter";
import Search from "../screens/Search";

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="HomeScreen" component={Home} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="ProfileCenter" component={ProfileCenter} />
    </Stack.Navigator>
  );
};

const ClientStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="ClientScreen" component={Client} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Search" component={Search} />
    </Stack.Navigator>
  );
};

const CenterStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="CentersScreen" component={Centers} />
      <Stack.Screen name="ProfileCenter" component={ProfileCenter} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Search" component={Search} />
    </Stack.Navigator>
  );
};

const AppointmentsStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="AppointmentsScreen" component={Appointments} />
    </Stack.Navigator>
  );
};

let HeaderNav = ({ navigation }) => {
  return <Header />;
};
const NavigationApp = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color }) => {
          let iconName;
          let iconColor;
          if (route.name === "Home") {
            iconName = "home";
            iconColor = focused ? `${globalstyle.Maincolor.color}` : "gray";
          } else if (route.name === "Client") {
            iconName = "user";
            iconColor = focused ? `${globalstyle.Maincolor.color}` : "gray";
          } else if (route.name === "Centers") {
            iconName = "users";
            iconColor = focused ? `${globalstyle.Maincolor.color}` : "gray";
          } else if (route.name === "Appointments") {
            iconName = "calendar";
            iconColor = focused ? `${globalstyle.Maincolor.color}` : "gray";
          }

          return (
            <View>
              <FontAwesome name={iconName} size={24} color={iconColor} />
            </View>
          );
        },
        tabBarLabelStyle: {
          display: "flex",
          color: "gray",
          marginBottom: 5,
        },
        tabBarStyle: {
          position: "absolute",
          bottom: 0,
          left: 0,
          right: 0,
          elevation: 0,
          backgroundColor: "white",
          borderTopColor: "lightgray",
          borderWidth: 1,
          height: 60,
          paddingBottom: 3,
        },
        headerShown: false,
        tabBarLabel: ({ focused, color }) => {
          let label;

          if (route.name === "Home") {
            label = "Home";
          } else if (route.name === "Client") {
            label = "Clinics";
          } else if (route.name === "Centers") {
            label = "Centers";
          } else if (route.name === "Appointments") {
            label = "Appointments";
          }

          return (
            <Text
              style={{
                color: focused ? `${globalstyle.Maincolor.color}` : "gray",
              }}
            >
              {label}
            </Text>
          );
        },
      })}
    >
      <Tab.Screen
        screenOptions={{ headerShown: false }}
        name="Home"
        component={HomeStack}
        options={{ unmountOnBlur: true }}
      />
      <Tab.Screen
        screenOptions={{ headerShown: false }}
        name="Client"
        component={ClientStack}
        options={{ unmountOnBlur: true }}
      />
      <Tab.Screen
        screenOptions={{ headerShown: false }}
        name="Centers"
        component={CenterStack}
        options={{ unmountOnBlur: true }}
      />
      <Tab.Screen
        screenOptions={{ headerShown: false }}
        name="Appointments"
        component={AppointmentsStack}
        options={{ unmountOnBlur: true }}
      />
    </Tab.Navigator>
  );
};

export default NavigationApp;
