import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import NavigationApp from "./NavigationApp";
import Myscreen from "../screens/Myscreen";
import Header from "../components/Header";
import CustomDrawerContent from "../components/CustomDrawerContent"; // Import the custom drawer content component
import Settings from "../screens/Settings";

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator
      drawerContent={() => <CustomDrawerContent />} // Set your custom drawer content component
      screenOptions={{
        header: () => <Header />, // Set your Header component as the header for all screens in the drawer
      }}
    >
      <Drawer.Screen
        name="MainApp"
        component={NavigationApp}
        options={{
          headerShown: true, // Show the header for the MainApp screen
        }}
      />
      <Drawer.Screen
        name="Myscreen"
        component={Myscreen}
        options={{
          headerShown: false,
        }}
      />
      <Drawer.Screen
        name="Settings"
        component={Settings}
        options={{
          headerShown: false,
        }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
