import { StyleSheet } from "react-native";

export let globalstyle = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  textTile: {
    fontFamily: "nunito-bold",
    fontSize: 18,
    color: "#333",
  },

  phragraph: {
    marginVertical: 8,
    lineHeight: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: "#ddd",
    padding: 10,
    fontSize: 18,
    borderRadius: 6,
  },
  Maincolor: {
    color: "#1EB290",
  },
});
