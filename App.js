import { View, Text } from "react-native";
import React from "react";
import MainFile from "./MainFile";
import { Fragment, useEffect, useState } from "react";
import { Provider, useSelector } from "react-redux"; // Import the Provider component
import Store from "./store"; // Import your Redux store

const App = () => {
  return (
    <Provider store={Store}>
      {/* Wrap your entire App component with the Provider */}
      <Fragment>
        <MainFile />
      </Fragment>
    </Provider>
  );
};

export default App;
